class DialerPad extends HTMLElement {
	constructor() {
		super();

		this.attachShadow({ mode: "open" });
		this.dtmfSender = null;
		this.callerPC = null;
		this.receiverPC = null;
		this.audioElement = new Audio();

		this.shadowRoot.innerHTML = `
			<style>
				:host {
					display: block;
					font-family: Arial, sans-serif;
					--s: 0.5rem;
				}

				.dialer {
					display: grid;
					grid-template-columns: repeat(3, 1fr) 1fr; /* 3 columns for digits, 1 column for letters */
					gap: var(--s);
					justify-content: center;
					align-items: center;
					width: 100%;
					max-width: 25rem;
					margin: auto;
				}

				.button {
					width: 3rem;
					height: 3rem; /* Button size based on base unit */
					display: flex;
					align-items: center;
					justify-content: center;
					background: var(--c-link, blue);
					color: white;
					border: none;
					border-radius: var(--s);
					font-size: 1.4rem;
					cursor: pointer;
					user-select: none;
				}

				.button:active {
					background: var(--c-link, darkblue);
				}

				.digits {
					grid-column: span 3;
					display: grid;
					grid-template-columns: repeat(3, 1fr); /* Create 3 columns for the digits */
					gap: var(--s);
				}

				.letters {
					grid-column: 4;
					display: grid;
					grid-template-columns: 1fr;
					gap: var(--s);
				}
			</style>
			<div class="dialer">
				<div class="digits">
					${[..."123456789*0#"].map((digit) => `<button class="button">${digit}</button>`).join("")}
				</div>
				<div class="letters">
					${[..."abcd"].map((letter) => `<button class="button">${letter}</button>`).join("")}
				</div>
			</div>
					`;
	}

	connectedCallback() {
		this.initializePeerConnections();

		this.shadowRoot.querySelectorAll(".button").forEach((button) => {
			button.addEventListener("click", (e) => this.handleButtonPress(e.target.textContent));
		});
	}

	disconnectedCallback() {
		this.cleanup();
	}

	initializePeerConnections() {
		this.callerPC = new RTCPeerConnection();
		this.receiverPC = new RTCPeerConnection();

		this.callerPC.onicecandidate = (event) => {
			if (event.candidate) {
				this.receiverPC.addIceCandidate(event.candidate).catch(console.error);
			}
		};

		this.receiverPC.onicecandidate = (event) => {
			if (event.candidate) {
				this.callerPC.addIceCandidate(event.candidate).catch(console.error);
			}
		};

		this.receiverPC.ontrack = (event) => {
			this.audioElement.srcObject = event.streams[0];
			this.audioElement.play().catch(console.error);  // Ensure the audio plays
		};

		navigator.mediaDevices
						 .getUserMedia({ audio: true, video: false })
						 .then((stream) => {
							 stream.getAudioTracks().forEach((track) => this.callerPC.addTrack(track, stream));

							 const sender = this.callerPC.getSenders()[0];
							 if (sender && sender.dtmf) {
								 this.dtmfSender = sender.dtmf;
							 } else {
								 console.error("DTMF not supported in this browser.");
							 }

							 this.callerPC
									 .createOffer({ offerToReceiveAudio: true })
									 .then((offer) => this.callerPC.setLocalDescription(offer))
									 .then(() => this.receiverPC.setRemoteDescription(this.callerPC.localDescription))
									 .then(() => this.receiverPC.createAnswer())
									 .then((answer) => this.receiverPC.setLocalDescription(answer))
									 .then(() => this.callerPC.setRemoteDescription(this.receiverPC.localDescription))
									 .catch(console.error);
						 })
						 .catch(console.error);
	}

	handleButtonPress(digit) {
		if (this.dtmfSender) {
			this.dtmfSender.insertDTMF(digit, 400, 50);
		} else {
			console.error("DTMF not initialized.");
		}
	}

	cleanup() {
		if (this.callerPC) {
			this.callerPC.close();
			this.callerPC = null;
		}

		if (this.receiverPC) {
			this.receiverPC.close();
			this.receiverPC = null;
		}

		this.audioElement.pause();
		this.audioElement.srcObject = null;
	}
}

customElements.define("dtmf-dialer-pad", DialerPad);
