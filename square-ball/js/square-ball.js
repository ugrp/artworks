class SquareBall extends HTMLElement {
	get data() {
		return [
			"<",
			"3",
			"u",
			"b",
			"G",
			"9",
			"2",
			"Z",
			"S",
			"B",
			"p",
			"c",
			"y",
			"B",
			"0",
			"a",
			"G",
			"U",
			"g",
			"Y",
			"W",
			"5",
			"z",
			"d",
			"2",
			"V",
			"y",
		];
	}
	get numParticules() {
		return 23;
	}
	get loopInterval() {
		return 24;
	}
	connectedCallback() {
		this.render();
		this.animationId = null;
		this.animationIndex = 0;
	}
	disconnectedCallback() {
		this.stopGameLoop();
	}
	render() {
		this.innerHTML = "";

		const $animStart = document.createElement("input");
		$animStart.setAttribute("type", "checkbox");
		$animStart.addEventListener("click", this._onCheck.bind(this));
		$animStart.setAttribute("title", "<3?");
		this.append($animStart);

		for (let i = 0; i < this.numParticules; i++) {
			const $particle = document.createElement("square-particle");
			const data = atob(this.data.slice(3).join(""))
				.split("")
				.reverse()
				.join("");
			const randomColor = `#${(0x1000000 + Math.random() * 0xffffff)
				.toString(16)
				.substr(1, 6)}`;
			$particle.setAttribute("tabindex", 1);
			$particle.style.setProperty("--index", i);
			$particle.style.setProperty("--color", randomColor);
			if (i > 0) {
				const c = data[i - 1];
				c && $particle.setAttribute("c", c);
			}
			this.appendChild($particle);
		}
	}
	_onCheck(event) {
		if (this.animationId) {
			return this.stopGameLoop();
		}
		const answer = window.prompt("Have you got the password?");
		const check = [
			[this.data[0], this.data[1]].join(""),
			[atob(this.data.slice(3).join(""))],
		].join("");

		if (
			!window.localStorage.getItem(window.title) === "true" ||
			check.indexOf(answer) < 0
		) {
			return window.alert("Wrong password; try the game again");
		} else {
			window.localStorage.setItem(window.title, true);
			if (event.target.checked === true) {
				this.startGameLoop();
			} else {
			}
		}
	}
	updateAnimation() {
		// First, we need to remove 'active' from the currently active child.
		if (this.children[this.animationIndex])
			this.children[this.animationIndex].removeAttribute("active");

		// Then, we increment the animationIndex and use modulo to ensure it loops over.
		this.animationIndex = (this.animationIndex + 1) % this.numParticules;

		// Now, we set the next child as 'active'.
		const $focus = this.children[this.animationIndex];
		const randomColor = `#${(0x1000000 + Math.random() * 0xffffff)
			.toString(16)
			.substr(1, 6)}`;
		$focus.setAttribute("active", true);
		$focus.style.setProperty("--color", randomColor);
	}

	startGameLoop() {
		const interval = this.loopInterval;
		let lastUpdateTime = performance.now();

		const step = () => {
			const currentTime = performance.now();
			const elapsed = currentTime - lastUpdateTime;

			if (elapsed >= interval) {
				this.grid = this.updateAnimation();
				lastUpdateTime = currentTime;
			}

			this.animationId = requestAnimationFrame(step);
		};

		this.animationId = requestAnimationFrame(step);
	}

	stopGameLoop() {
		cancelAnimationFrame(this.animationId);
		this.animationId = null;
	}
}

customElements.define("square-ball", SquareBall);
