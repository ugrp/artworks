# artworks

A repository for prototyping and research with web projects.

## development

Each "artwork" is in its own folder.

Instructions to run each of them should be found in each `readme` file.

If none exist open the `index.html` file in a web browser, or run a web server
with `npx serve .` for practicity.
