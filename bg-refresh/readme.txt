Dev with `npx serve .` or open `index.html` file in new browser tab.

- meta: refresh page every one second
- document.title is a random string from https://stackoverflow.com/questions/19448680/how-to-get-a-random-character-from-a-range-in-javascript
- html.backgroundColor is a random color from https://stackoverflow.com/questions/1152024/best-way-to-generate-a-random-color-in-javascript#1152508

