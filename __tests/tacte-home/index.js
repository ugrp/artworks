import { pnoise3, vertexShader, fragmentShader} from "./displacement-shader.js"

console.log("Tacte")

AFRAME.registerComponent("log", {
	init: function () {
		// Event for when the scene is loaded
		this.el.addEventListener("loaded", () => {
			this.el.setAttribute("is-loaded", true);
			document.querySelector("body").setAttribute("is-loaded", true);
		});
	}
});


AFRAME.registerShader('displacement', {
	schema: {
		timeMsec: { type: 'time', is: 'uniform' }
	},
	vertexShader: pnoise3 + vertexShader,
	fragmentShader
});
