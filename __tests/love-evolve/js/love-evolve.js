class LoveEvolve extends HTMLElement {
	connectedCallback() {
		this.render();
	}

	render() {
		const numParticules = 10;
		const container = this;

		for (let i = 0; i < numParticules; i++) {
			const $particle = document.createElement("love-particule");
			$particle.setAttribute("tabindex", 1);
			$particle.style.setProperty("--index", i);
			$particle.style.setProperty("--animation-delay", `${i * 0.1}s`);
			container.appendChild($particle);
		}
	}
}

customElements.define("love-evolve", LoveEvolve);
