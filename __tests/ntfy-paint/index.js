export class ArtworkPaint extends HTMLElement {
	getRandomColor() {
		// https://stackoverflow.com/questions/1484506/random-color-generator
		return (
			"#" +
			Math.floor(Math.random() * 16777215)
				.toString(16)
				.padStart(6, "0")
		);
	}
	constructor() {
		super();
		this.setupCanvas();
		this.setupDrawing();
		window.addEventListener("resize", this.handleResize.bind(this));
	}

	setupCanvas() {
		this.canvas = document.createElement("canvas");
		this.appendChild(this.canvas);
		this.context = this.canvas.getContext("2d");
		this.handleResize();
	}

	setupDrawing() {
		this.isDrawing = false;

		this.canvas.addEventListener("mousedown", this.startDrawing.bind(this));
		this.canvas.addEventListener("mousemove", this.draw.bind(this));
		this.canvas.addEventListener("mouseup", this.endDrawing.bind(this));
		this.canvas.addEventListener("mouseout", this.endDrawing.bind(this));

		this.canvas.addEventListener("touchstart", this.startDrawing.bind(this));
		this.canvas.addEventListener("touchmove", this.draw.bind(this));
		this.canvas.addEventListener("touchend", this.endDrawing.bind(this));
	}

	startDrawing(e) {
		this.isDrawing = true;
		const { offsetX, offsetY } = this.getMousePosition(e);
		this.context.beginPath();
		this.context.moveTo(offsetX, offsetY);
	}

	draw(e) {
		if (!this.isDrawing) return;
		const { offsetX, offsetY } = this.getMousePosition(e);
		this.context.lineTo(offsetX, offsetY);
		this.context.stroke();
		this.context.strokeStyle = this.getRandomColor();
	}

	endDrawing() {
		this.isDrawing = false;
	}

	getMousePosition(e) {
		let clientX, clientY;
		if (e.type.startsWith("touch")) {
			clientX = e.touches[0].clientX;
			clientY = e.touches[0].clientY;
		} else {
			clientX = e.clientX;
			clientY = e.clientY;
		}
		const rect = this.canvas.getBoundingClientRect();
		return {
			offsetX: clientX - rect.left,
			offsetY: clientY - rect.top,
		};
	}

	toDataURL() {
		return this.canvas.toDataURL();
	}

	clearCanvas() {
		this.context.clearRect(0, 0, this.canvas.width, this.canvas.height);
	}

	handleResize() {
		this.canvas.width = window.innerWidth;
		this.canvas.height = window.innerHeight;
	}
}

export class ArtworkPublish extends HTMLElement {
	get baseTopic() {
		return this.getAttribute("base-topic") || "ugrp_artworks_paint";
	}
	get topic() {
		return this.getAttribute("topic") || "general";
	}
	get artworkPaint() {
		return document.querySelector("artwork-paint");
	}

	constructor() {
		super();
		const button = document.createElement("button");
		button.textContent = "Broadcast";
		button.addEventListener("click", this.publishDrawing.bind(this));
		this.appendChild(button);
	}

	publishDrawing() {
		const canvas = this.artworkPaint.canvas;
		canvas.toBlob((blob) => {
			const filename = "drawing.png";
			fetch(`https://ntfy.sh/${this.baseTopic}-${this.topic}`, {
				method: "PUT",
				body: blob,
				headers: {
					Filename: filename,
				},
			})
				.then((response) => {
					if (!response.ok) {
						throw new Error("Failed to publish drawing");
					}
					console.log("Drawing published successfully");
					this.artworkPaint.clearCanvas();
				})
				.catch((error) => {
					console.error("Error publishing drawing:", error);
				});
		});
	}
}

export class ArtworkSubscribe extends HTMLElement {
	get baseTopic() {
		return this.getAttribute("base-topic") || "ugrp_artworks_paint";
	}

	get topic() {
		return this.getAttribute("topic") || "general";
	}

	connectedCallback() {
		this.eventSource = new EventSource(
			`https://ntfy.sh/${this.baseTopic}-${this.topic}/sse`,
		);
		this.eventSource.onmessage = this.handleMessage.bind(this);
	}

	disconnectedCallback() {
		if (this.eventSource) {
			this.eventSource.close();
		}
	}

	handleMessage(event) {
		const data = JSON.parse(event.data);
		const imgElement = document.createElement("img");
		imgElement.setAttribute("alt", data.message);
		imgElement.setAttribute("src", data.attachment.url);
		this.append(imgElement);
	}
}
