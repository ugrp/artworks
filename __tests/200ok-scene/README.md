# 200okrecords

Starts with the `index.html` file

## Dependencies

The site is deployed as a gitlab page, using the config in the `.gitlab-ci.yml` file.

There is a dependency on several libraries, mostly web components:

- [discogs-elements](https://gitlab.com/publicservices/web-components/discogs-elements)
- [radio4000-player](https://player.radio4000.com)
- [@sctlib/matrix-room-element](https://gitlab.com/sctlib/matrix-room-element)
- [a-frame](https://aframe.io/)

## known issues

### Dicogs data not loading

Because of the rate limit of discogs' API, if the page is loaded too many times,
it will sometimes hang, because we're not allowed to get the data again

Possible solution: cache data to local storage, and check once in a white (there
should not be too many updates to the discogs data anyways).
