export default class LifeGame extends HTMLElement {
	get loopInterval() {
		return 100;
	}
	get birthChance() {
		return 0.5;
	}
	constructor() {
		super();
		this.resizeObserver = new ResizeObserver(this.handleResize.bind(this));
		this.animationId = null;
	}
	connectedCallback() {
		this.newGrid();
		this.setStyles();
		this.renderGrid();
		this.startGameLoop();
		this.resizeObserver.observe(this);
	}
	disconnectedCallback() {
		this.stopGameLoop();
		this.resizeObserver.disconnect();
	}
	newGrid() {
		this.size = Math.round(Math.max(this.clientHeight, this.clientWidth) / 15);
		this.grid = this.createGrid();
	}
	handleResize() {
		this.newGrid();
		this.setStyles();
		this.renderGrid();
	}
	setStyles() {
		this.style.setProperty("--size-grid", this.size);
		/* this.style.gridTemplate = `repeat(${this.size}, 1fr) / repeat(${this.size}, 1fr)`; */
	}

	startGameLoop() {
		const interval = this.loopInterval;
		let lastUpdateTime = performance.now();

		const step = () => {
			const currentTime = performance.now();
			const elapsed = currentTime - lastUpdateTime;

			if (elapsed >= interval) {
				this.grid = this.updateGrid();
				lastUpdateTime = currentTime;
			}

			this.animationId = requestAnimationFrame(step);
		};

		this.animationId = requestAnimationFrame(step);
	}

	stopGameLoop() {
		cancelAnimationFrame(this.animationId);
	}

	createGrid() {
		let grid = [];
		for (let i = 0; i < this.size; i++) {
			let row = [];
			for (let j = 0; j < this.size; j++) {
				const cellAlive = Math.random() > this.birthChance;
				row.push(cellAlive);
			}
			grid.push(row);
		}
		return grid;
	}

	renderGrid() {
		this.innerHTML = "";
		this.grid.forEach((row, rowIndex) => {
			row.forEach((cell, cellIndex) => {
				const $cell = document.createElement("life-cell");
				$cell.setAttribute("alive", cell ? "true" : "false");
				$cell.setAttribute("row", rowIndex);
				$cell.setAttribute("column", cellIndex);
				this.appendChild($cell);
			});
		});
	}

	updateGrid() {
		const gridSnapshot = JSON.parse(JSON.stringify(this.grid));
		const newGrid = [];

		for (let i = 0; i < this.size; i++) {
			const newRow = [];

			for (let j = 0; j < this.size; j++) {
				const liveNeighbors = this.countLiveNeighbors(i, j, gridSnapshot);
				const currentAlive = this.grid[i][j];
				let newAlive = false;
				if (currentAlive) {
					if (liveNeighbors === 2 || liveNeighbors === 3) {
						newAlive = true;
					}
				} else {
					if (liveNeighbors === 3) {
						newAlive = true;
					}
				}
				const aliveSelector = `life-cell[row="${i}"][column="${j}"]`;
				const $newAlive = this.querySelector(aliveSelector);
				$newAlive.setAttribute("alive", newAlive ? "true" : "false");
				newRow.push(newAlive);
			}
			newGrid.push(newRow);
		}
		return newGrid;
	}

	countLiveNeighbors(x, y, grid) {
		let count = 0;
		for (let dx = -1; dx <= 1; dx++) {
			for (let dy = -1; dy <= 1; dy++) {
				let nx = x + dx;
				let ny = y + dy;
				if (nx >= 0 && nx < this.size && ny >= 0 && ny < this.size) {
					count += grid[nx][ny] ? 1 : 0;
				}
			}
		}
		count -= this.grid[x][y] ? 1 : 0;
		return count;
	}
}

customElements.define("life-game", LifeGame);
