export default class OriginalVortex extends HTMLElement {
	connectedCallback() {
		this.render();
		this.afterRender();
	}

	render() {
		const numParticles = 30;
		const container = this;

		const checkbox = document.createElement("input");
		checkbox.setAttribute("type", "checkbox");
		checkbox.setAttribute("tabindex", 1);
		checkbox.checked = false;

		this.append(checkbox);

		for (let i = 0; i < numParticles; i++) {
			const particle = document.createElement("vortex-particle");
			particle.style.setProperty("--index", i);
			particle.style.setProperty("--color", this.getRandomColor());
			particle.setAttribute("tabindex", 1);
			if (i % 5 === 0) {
				const iframe = document.createElement("iframe");
				iframe.setAttribute(
					"src",
					"https://ugrp.gitlab.io/artworks/char-feelings/"
				);
				iframe.setAttribute("tabindex", 0);
				particle.append(iframe);
			}
			container.appendChild(particle);
		}

		const result = document.createElement("a");
		result.setAttribute(
			"href",
			"https://example.org?try_this=https://ugrp.gitlab.io/html-css-game"
		);
		result.setAttribute("tabindex", 1);
		result.innerText = "?";
		result.addEventListener("click", (e) => {
			if (
				!window.confirm(
					"You completed this artwork! Congratulations! Are you ready to move on to the next one?"
				)
			) {
				e.preventDefault();
			}
		});

		const iframe = document.createElement("iframe");
		iframe.setAttribute(
			"src",
			"https://ugrp.gitlab.io/artworks/char-feelings/"
		);
		iframe.setAttribute("tabindex", 1);
		result.append(iframe);
		container.children[numParticles].append(result);
	}
	afterRender() {
		this.querySelectorAll("vortex-particle").forEach((particle) => {
			particle.setAttribute("ready", true);
		});
	}
	getRandomColor() {
		return (
			"#" +
			Math.floor(Math.random() * 16777215)
				.toString(16)
				.padStart(6, "0")
		);
	}
	solve() {
		const check = document.querySelector("input[type='checkbox']");
		check.checked = !check.checked;
		if (check.checked) {
			document.querySelector("vortex-particle").focus();
		}
	}
}

customElements.define("original-vortex", OriginalVortex);
