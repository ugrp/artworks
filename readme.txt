# artworks

This repository and website contains mostly html/css/js prototypes of explorative web applications.

## Examples

In a new tab with the following (data) content in the URL:
```
data:text/html;charset=utf8,<body style="margin: 0; overflow: hidden;"><iframe src="https://ugrp.gitlab.io/artworks/mx-system/?mono=true&message=STwzVQ==" style="width:100vw; height: 100vh; border: none;"></iframe></body>
```
