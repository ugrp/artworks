const i18n = {
	copy: "Copy share URL",
	copied: "Share URL copied!"
}

export default class siteInterface extends HTMLElement {
	get shareUrl() {
		const {
			text
		} = this.state
		const {origin, pathname} = window.location
		return `${origin}${pathname}#m=${btoa(text)}`
	}
	constructor() {
		super()
		this.state = {}
	}
	connectedCallback() {
		if (!window.location.hash) {
			this.render()
		}
	}
	render() {
		const shareForm = this.createShareForm()
		this.replaceChildren(shareForm)
	}
	createShareForm() {
		const text = this.createText()
		const share = this.createShare()

		const form = document.createElement("form")
		form.addEventListener("submit", (event) => event.preventDefault())
		form.append(text, share)
		return form
	}
	createText() {
		const input = document.createElement("input")
		input.setAttribute("name", "text")
		input.setAttribute("placeholder", "Custom text")
		input.setAttribute("max", 20)
		input.addEventListener("input", this.onInput.bind(this))
		return input
	}
	createShare() {
		const button = document.createElement("button")
		button.textContent = i18n.copy
		button.addEventListener("click", this.onShare.bind(this))
		return button
	}
	onInput(event) {
		this.state = {
			...this.state,
			[event.target.name]: event.target.value,
		}
	}
	onShare(event) {
		navigator.clipboard.writeText(this.shareUrl);
		event.target.innerText = i18n.copied
		setTimeout(() => {
			event.target.innerText = i18n.copy
		}, 1000)
	}
}
