import siteInterface from "./site-interface.js"

const componentDefinitions = {
	"site-interface": siteInterface
}

/* auto define all components, if in browser */
export function defineComponents(components) {
	Object.entries(components).map(([cTag, cDef]) => {
		if (!customElements.get(cTag)) {
			customElements.define(cTag, cDef);
		}
	});
}

defineComponents(componentDefinitions);
