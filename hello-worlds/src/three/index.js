import * as THREE from 'three'
import { OrbitControls } from 'three/addons/controls/OrbitControls.js'
import { Timer } from 'three/addons/misc/Timer.js'
import { FontLoader } from 'three/addons/loaders/FontLoader.js'
import { TextGeometry } from 'three/addons/geometries/TextGeometry.js'
/* import GUI from 'lil-gui' */

// Define color themes for light and dark modes
const themes = {
	light: {
		background: "white",
		fog: "blue",
		floor: "cyan",
		walls: "hotpink",
		roof: "limegreen",
		points: "#ff00ff",
		lines: "#00ff00",
		ambient: "#ffffff",
		directional: "#ffffff",
		text: "rebeccapurple"
	},
	dark: {
		background: "black",
		fog: "orange",
		floor: "slateblue",
		walls: "limegreen",
		roof: "hotpink",
		points: "#00ff00",
		lines: "#ff00ff",
		ambient: "#ffffff",
		directional: "#ffffff",
		text: "cyan"
	}
}

// Select active theme based on the user's preference
const activeTheme = window.matchMedia('(prefers-color-scheme: dark)').matches
	? themes.dark
	: themes.light

THREE.DefaultLoadingManager.onLoad = () => {
	document.querySelector("body").setAttribute("is-loaded", true)
}

/**
 * Base
 */
// Debug
/* const gui = new GUI() */

const searchParams = new URLSearchParams(window.location.hash.slice(1))
const userData = {
	message: searchParams.get("m") ? atob(searchParams.get("m")) : "Hello\nWorlds!"
}

// Canvas
const canvas = document.querySelector('canvas.webgl')

// Scene
const scene = new THREE.Scene()
scene.background = new THREE.Color(activeTheme.background);

scene.fog = new THREE.Fog(activeTheme.fog, 100, 200);

/* scene helpers */
const axesHelper = new THREE.AxesHelper(5);
/* scene.add(axesHelper); */

/*
	 texture loader
 */
const textureLoader = new THREE.TextureLoader()
const floorAlphaTexture = textureLoader.load("./static/floor/alpha.jpg")

/*
	 font loader
 */
const loader = new FontLoader();
loader.load("./static/fonts/Parisienne/Parisienne_Regular.json", (font) => {
	insertText(font)
});

/**
 * House
 */
const floorSizes = {
	width: 20,
	height: 20
}
const floor = new THREE.Mesh(
	new THREE.PlaneGeometry(floorSizes.width, floorSizes.height),
	new THREE.MeshStandardMaterial({
		color: activeTheme.floor,
		alphaMap: floorAlphaTexture,
		transparent: true,
		side: THREE.DoubleSide,
		alphaTest: 0.5
	})
)

floor.position.y = -0.001
floor.rotation.x = - Math.PI * 0.5
scene.add(floor)

const house = new THREE.Group()
scene.add(house)

const wallSizes = {
	height: 3,
	width: 6,
	depth: 5,
}
const walls = new THREE.Mesh(
	new THREE.BoxGeometry(wallSizes.width, wallSizes.height, wallSizes.depth),
	new THREE.MeshStandardMaterial({ color: activeTheme.walls, fog: false })
)
walls.position.y = wallSizes.height / 2
house.add(walls)

const roofSizes = {
	radius: wallSizes.width,
	height: 1.5,
	segments: 4,
}
const roofGeometry = new THREE.ConeGeometry(roofSizes.radius, roofSizes.height, roofSizes.segments)
const roof = new THREE.Mesh(
	roofGeometry,
	new THREE.MeshStandardMaterial({ color: activeTheme.roof, fog: false })
)
roof.position.y = wallSizes.height + (roofSizes.height * 0.5)
roof.rotation.y = Math.PI * 0.25
house.add(roof)

/* add point for stars */
const vertices = [];
for (let i = 0; i < 500; i++) {
	const x = THREE.MathUtils.randFloatSpread(300);
	const y = THREE.MathUtils.randFloatSpread(300);
	const z = THREE.MathUtils.randFloatSpread(300);
	vertices.push(x, y, z);
}

const geometry = new THREE.BufferGeometry();
geometry.setAttribute('position', new THREE.Float32BufferAttribute(vertices, 3));
const pointsMaterial = new THREE.PointsMaterial({ color: activeTheme.points });
const points = new THREE.Points(geometry, pointsMaterial);
scene.add(points);

/* lines between points */
const distanceThreshold = 30;
function getDistance(p1, p2) {
	const dx = p1.x - p2.x;
	const dy = p1.y - p2.y;
	const dz = p1.z - p2.z;
	return Math.sqrt(dx * dx + dy * dy + dz * dz);
}

const lineSegments = new THREE.Group();
const positions = geometry.attributes.position.array;
for (let i = 0; i < positions.length; i += 3) {
	for (let j = i + 3; j < positions.length; j += 3) {
		const p1 = new THREE.Vector3(positions[i], positions[i + 1], positions[i + 2]);
		const p2 = new THREE.Vector3(positions[j], positions[j + 1], positions[j + 2]);
		if (getDistance(p1, p2) < distanceThreshold) {
			const pointsArray = new Float32Array([
				p1.x, p1.y, p1.z,
				p2.x, p2.y, p2.z
			]);
			const lineGeometry = new THREE.BufferGeometry();
			lineGeometry.setAttribute('position', new THREE.Float32BufferAttribute(pointsArray, 3));
			const lineMaterial = new THREE.LineBasicMaterial({ color: activeTheme.lines });
			const line = new THREE.Line(lineGeometry, lineMaterial);
			lineSegments.add(line);
		}
	}
}
scene.add(lineSegments);

/**
 * Lights
 */
const ambientLight = new THREE.AmbientLight(activeTheme.ambient, 0.5)
scene.add(ambientLight)

const directionalLight = new THREE.DirectionalLight(activeTheme.directional, 1.5)
directionalLight.position.set(3, 2, -8)
scene.add(directionalLight)

/**
 * Sizes
 */
const canvasSizes = {
	width: window.innerWidth,
	height: window.innerHeight
}

window.addEventListener('resize', () => {
	canvasSizes.width = window.innerWidth
	canvasSizes.height = window.innerHeight
	camera.aspect = canvasSizes.width / canvasSizes.height
	camera.updateProjectionMatrix()
	renderer.setSize(canvasSizes.width, canvasSizes.height)
	renderer.setPixelRatio(Math.min(window.devicePixelRatio, 2))
})

/**
 * Camera
 */
const camera = new THREE.PerspectiveCamera(75, canvasSizes.width / canvasSizes.height, 0.1, 300)
camera.position.set(0, 20, 40)
camera.lookAt(house.position)
scene.add(camera)

const controls = new OrbitControls(camera, canvas)
controls.enableDamping = true

/**
 * Renderer
 */
const renderer = new THREE.WebGLRenderer({ canvas: canvas })
renderer.setSize(canvasSizes.width, canvasSizes.height)
renderer.setPixelRatio(Math.min(window.devicePixelRatio, 2))

/* Effects */
const insertText = (font) => {
	const textGeometry = new TextGeometry(`${userData.message}`, {
		font: font,
		size: 5,
		depth: 0.4,
		curveSegments: 10,
		bevelEnabled: true,
		bevelThickness: 0.1,
		bevelSize: 0.1,
		bevelOffset: 0.1,
		bevelSegments: 5
	});
	const textMaterial = new THREE.MeshBasicMaterial({
		color: activeTheme.text,
		fog: false
	})
	const text = new THREE.Mesh(textGeometry, textMaterial)
	scene.add(text)
	textGeometry.computeBoundingBox()
	text.position.x = textGeometry.boundingBox.max.x * -0.5
	text.position.y = wallSizes.height * 2 + (textGeometry.boundingBox.max.y + Math.abs(textGeometry.boundingBox.min.y) * 0.5)
	text.position.z = wallSizes.width * -2
	text.rotation.y = Math.PI * 2
}

/**
 * Animate
 */
const timer = new Timer()

const tick = () => {
	timer.update()
	controls.update()
	renderer.render(scene, camera)
	window.requestAnimationFrame(tick)
}

tick()
