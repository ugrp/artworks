import {
	getRandomExistance,
	getRandomChar,
	getRandomColor,
} from '../utils.js'

export default class CustomColumn extends HTMLElement {
	constructor() {
		super()
		this.addEventListener('charRemoved', this.handleCharRemoved)
		this.addEventListener('charRemoved', this.handleCharDeath)
	}
	connectedCallback() {
		this.setup()
	}

	/* setup an initial column filled with enough chars */
	setup() {
		this.innerHTML = ''
		for (let i = 0; i < this.getAttribute('size'); i++) {
			this.createChar({
				screen: true,
				/* death: true, */
				update: true,
				animate: '3d',
				x: Number(this.getAttribute('index')),
				y: i,
				z: this.getAttribute('z'),
				/* char is set as last, as it is observed */
				char: getRandomChar(),
			})
		}
	}
	createChar(state) {
		const $el = document.createElement('custom-char')
		Object.keys(state).forEach(attr => {
			$el.setAttribute(attr, state[attr])
		})
		this.append($el)
	}

	/* setup an initial column filled with enough chars */
	update() {
		const $el = document.createElement('custom-char')
		this.prepend($el)
	}

	handleCharRemoved(event) {
		this.removeChild(event.target)
	}
	handleCharDeath({detail}) {
		if (detail.death === "true") {
			/* console.log('detail', detail) */
			detail.char = getRandomChar()
			this.createChar(detail)
		}
	}
}
