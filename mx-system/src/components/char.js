import {
	getRandomChar,
	getRandomColor,
	getRandomExistance,
} from '../utils.js'

export default class CustomChar extends HTMLElement {
	static get observedAttributes() {
		return ['char']
	}
	get existance() {
		const value = Number(this.getAttribute('existance'))
		return value
	}
	set existance(newValue) {
		return this.setAttribute('existance', newValue)
	}
	get x() {
		return Number(this.getAttribute('x'))
	}
	set x(newValue) {
		return this.setAttribute('x', newValue)
	}
	get y() {
		return Number(this.getAttribute('y'))
	}
	set y(newValue) {
		return this.setAttribute('y', newValue)
	}
	get z() {
		return Number(this.getAttribute('z'))
	}
	set z(newValue) {
		return this.setAttribute('z', newValue)
	}

	constructor() {
		super()
		this.addEventListener('keyup', this.handleKeyboard)
	}

	connectedCallback() {
		/* so DOM el can be focused with TAB, and js event not triggered */
		this.setAttribute('tabindex', 0)

		this.setAttribute('color', getRandomColor())

		if (!this.existance) {
			this.existance = getRandomExistance()
		}

		/* console.log(this.x, this.y, this.existance) */
		const life = this.existance + (this.x + this.y)
		const death = this.existance + (this.x + this.y) * 420

		if (!this.getAttribute('char')) {
			this.update()
		}

		if (this.getAttribute('update')) {
			setInterval(this.update, this.existance * 3)
		}

		if (this.existance) {
			/* "come to life with color" */
			setTimeout(() => {
				this.style.setProperty('--fg', this.getAttribute('color'))
			}, life)
			if (this.getAttribute('animate')) {
				this.style.setProperty('--existance', this.existance + 'ms')
			}
		}
		if (this.getAttribute('death')) {
			/* dispatch the DOM deletion event */
			setTimeout(() => {
				const charRemoved = new CustomEvent('charRemoved', {
					bubbles: true,
					detail: {
						char: this.getAttribute('char'),
						color: this.getAttribute('color'),
						death: this.getAttribute('death'),
						existance: this.existance,
						x: this.x,
						y: this.y,
						z: this.z,
						animate: this.getAttribute('animate'),
						update: this.getAttribute('update'),
						screen: this.getAttribute('screen'),
					}
				})
				this.dispatchEvent(charRemoved)
			}, death)
		}
	}
	disconnectedCallback() {
		this.removeEventListener('keyup', this.handleKeyboard)
		if (this.getAttribute('update')) {
			clearInterval(this.update)
		}
		if (this.getAttribute('death')) {
			/* dispatch the DOM deletion event */
			const charRemoved = new CustomEvent('charRespawn', {
				bubbles: true,
				detail: {
					char: this.getAttribute('char'),
					color: this.getAttribute('color'),
					death: this.getAttribute('death'),
					existance: this.existance,
				}
			})
		}
	}
	attributeChangedCallback(name, oldValue, newValue) {
		if (name === 'char' && this.getAttribute('screen')) {
			this.innerText = newValue
		}
	}
	update = () => {
		this.setAttribute('char', getRandomChar())
	}

	/* keybaord controls */
	handleKeyboard = (event) => {
		/* delegate eacape key event to app */
		if (event.key === 'Escape') {
			return
		}
		/* if we're in inspect mode, catch all event keys */
		if (!this.getAttribute('inspect')) {
			event.stopPropagation()
		}
		if (event.key === 'i') {
			this.toggleAttribute('inspect')
		}
		if (event.key === 'Delete') {
			this.remove()
		}
	}
}
