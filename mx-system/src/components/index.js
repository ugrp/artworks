import CustomChar from './char.js'
import CustomColumn from './column.js'

customElements.define('custom-char', CustomChar)
customElements.define('custom-column', CustomColumn)


export {
	CustomChar,
	CustomColumn,
}
