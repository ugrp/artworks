import { isBase64, modulo, getRandomExistance } from "../utils.js";

export default class AwMx extends HTMLElement {
	constructor() {
		super();
		this.addEventListener("keyup", this.handleKeyboard);
	}
	connectedCallback() {
		/* so DOM el can be focused with TAB, and js event not triggered;
		 1, because main app */
		this.setAttribute("tabindex", 1);

		/* setup initial state from params */
		const urlSearchParams = new URLSearchParams(window.location.search);
		const params = Object.fromEntries(urlSearchParams.entries());

		/* monochrome app */
		if (params.mono) {
			this.setAttribute("mono", true);
		}

		/* use btoa('my-message') to encrypt one */
		if (params.message) {
			if (isBase64(params.message)) {
				return this.setAttribute("message", atob(params.message));
			} else {
				return this.setAttribute("message", "###ERROR###");
			}
		}

		this.setup();
		window.addEventListener(
			"resize",
			() => {
				this.setup();
			},
			false
		);
	}

	/* when window is resized */
	setup() {
		const width = window.innerWidth;
		const height = window.innerHeight;

		/* gets the font size in px (whatever the use value is) */
		const rootFs = window
			.getComputedStyle(this, null)
			.getPropertyValue("font-size")
			.split("px")[0];
		/* use the root element fontsize, to calculate the grid render:
			 (coordinates) x = horizontal (column), y = vertical (row), z = depth (layer) */
		this.grid = {
			x: Math.floor(width / rootFs),
			y: Math.floor(height / rootFs),
			z: Number(this.getAttribute("z")) || 1,
		};
		this.setAttribute("x", this.grid.x);
		this.setAttribute("y", this.grid.y);
		this.render(this.grid);
		this.positionCursor();
		this.attachMessage(this.grid);
	}

	/* render the app columns */
	render({ x, y, z }) {
		this.innerHTML = "";
		const $frames = [];
		if (z) {
			for (let iZ = 0; iZ < z; iZ++) {
				$frames.push(
					this.createFrame({
						index: iZ,
						x,
						y,
					})
				);
			}
		}

		$frames.forEach(($frame) => {
			this.append($frame);
		});
	}
	createFrame({ index, x, y }) {
		const $frame = document.createElement("custom-frame");
		if (x) {
			for (let iX = 0; iX < x; iX++) {
				$frame.append(
					this.createColumn({
						size: y,
						index: iX,
						z: index,
					})
				);
			}
		}
		return $frame;
	}
	createColumn({ index, size, z }) {
		if (size) {
			const $el = document.createElement("custom-column");
			$el.setAttribute("size", size);
			$el.setAttribute("index", index);
			$el.setAttribute("z", z);
			return $el;
		}
	}

	/* somehow attach a message in the grid */
	attachMessage(grid) {
		console.log("attach!", grid);
	}

	/* cursor */
	buildCursorSelector({ x, y, z }) {
		return `custom-char[x='${x}'][y='${y}'][z='${z}']`;
	}
	focusCursor(position) {
		const charQuery = this.buildCursorSelector(position);
		const $char = this.querySelector(charQuery);
		$char.focus();
	}
	markChar(position) {
		const charQuery = this.buildCursorSelector(position);
		const $char = this.querySelector(charQuery);
		$char.toggleAttribute("marked");
	}
	unmarkChars() {
		const $chars = this.querySelectorAll("custom-char[marked]");
		$chars.forEach(($char) => $char.toggleAttribute("marked"));
	}
	positionCursor(
		newPos = {
			x: 0,
			y: 0,
			z: 0,
		}
	) {
		this.cursor = {
			x: modulo(newPos.x, this.grid.x),
			y: modulo(newPos.y, this.grid.y),
			z: modulo(newPos.z, this.grid.z),
		};
		console.log(newPos, this.grid, this.cursor);

		const activeCharQuery = `custom-char[cursor='true']`;
		const $activeChar = this.querySelector(activeCharQuery);
		$activeChar && $activeChar.removeAttribute("cursor");

		const charQuery = this.buildCursorSelector(this.cursor);
		const $char = this.querySelector(charQuery);
		$char && $char.setAttribute("cursor", true);
	}

	/* global keyboard events */
	handleKeyboard(event) {
		/* <ESC> will aways focus the app*/
		if (event.key === "Escape") {
			this.focus();
		}

		/* <C-M-RET> is submit with prompt */
		if (event.ctrlKey && event.altKey && event.code === "Enter") {
			const result = prompt();
			const submit = new CustomEvent("submitResult", {
				detail: {
					result,
					data: this.innerText,
				},
			});
			console.info(this, "submited result", submit);
			return this.dispatchEvent(submit);
		}

		/* <C-RET> will "submit" data to out of the app*/
		if (event.ctrlKey && event.key === "Enter") {
			const submit = new CustomEvent("submit", {
				detail: {
					data: this.innerText,
				},
			});
			console.info(this, "submited", submit);
			return this.dispatchEvent(submit);
		}

		/* <M-x> opens the command menu */
		if (event.altKey && event.key === "x") {
			console.info("Command menu", this);
		}

		/* focus char under cursor */
		if (event.key === "Enter") {
			this.focusCursor(this.cursor);
		}
		/* mark char undor cursor */
		if (event.key === "m") {
			this.markChar(this.cursor);
		}
		if (event.key === "U") {
			this.unmarkChars();
		}

		/* cursor movements */
		if (event.key === "n") {
			this.positionCursor({
				...this.cursor,
				y: this.cursor.y + 1,
			});
		}
		if (event.key === "p") {
			this.positionCursor({
				...this.cursor,
				y: this.cursor.y - 1,
			});
		}
		if (event.key === "f") {
			this.positionCursor({
				...this.cursor,
				x: this.cursor.x + 1,
			});
		}
		if (event.key === "b") {
			this.positionCursor({
				...this.cursor,
				x: this.cursor.x - 1,
			});
		}
	}
}
