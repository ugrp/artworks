/* some constants */
/* FACT-CHECK? the total number of unicodes in js */
const totalUnicodeChars = 65535


/**
 * Debounce functions for better performance
 * (c) 2021 Chris Ferdinandi, MIT License, https://gomakethings.com
 * @param  {Function} fn The function to debounce
 */
function debounce (fn) {
	// Setup a timer
	let timeout

	// Return a function to run debounced
	return function () {

		// Setup the arguments
		let context = this
		let args = arguments

		// If there's a timer, cancel it
		if (timeout) {
			window.cancelAnimationFrame(timeout)
		}

		// Setup the new requestAnimationFrame()
		timeout = window.requestAnimationFrame(function () {
			fn.apply(context, args)
		})
	}
}


/* base 64 string check */
const isBase64 = (str) => {
	try {
		return btoa(atob(str)) == str
	} catch (err) {
		return false
	}
}

/* a "real" modulo (only positive integer) */
const modulo = (number, mod) => {
	return ((number % mod) + mod) % mod
}


/* About getting/generating random things;
	 and "fake randomness" from their initial state */

// https://stackoverflow.com/questions/1484506/random-color-generator
const getRandomColor = (randSeed = Math.random()) => {
	return '#' + Math.floor(randSeed * 16777215).toString(16).padStart(6, '0')
}

/* returns an integer value in `ms`; an existance (duration) time */
const getRandomExistance = (randSeed = Math.random()) => {
	return Math.floor((randSeed + 1) * 500)
}

// https://stackoverflow.com/questions/19448680/how-to-get-a-random-character-from-a-range-in-javascript
const getRandomChar = (randSeed = Math.random()) => {
	return String.fromCharCode(Math.floor(randSeed * totalUnicodeChars))
}
const incrementRandomChar = (char) => {
	const currentCharCode = char.charCodeAt(0)
	const nextCharCode = currentCharCode + 1
	return String.fromCharCode(nextCharCode)
}

export {
	totalUnicodeChars,
	debounce,
	isBase64,
	modulo,

	getRandomChar,
	incrementRandomChar,
	getRandomColor,
	getRandomExistance,
}
