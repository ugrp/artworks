/* components */
import * as components from './components/index.js'

/* artworks/apps, using the components to do stuff */
import * as apps from './aw/index.js'

export {
	components,
	apps,
}
